Flat Table Import Handler 
-------------------------

A custom solr import handler that takes a flat mysql query and 

1. Merges rows with identical unique id
2. Merges property-value pairs into documents

A prerequisite for a correct merge is that rows with the same unique id appear diectly after each other.
Document insertion into Solr occur once a new unique id is found.
