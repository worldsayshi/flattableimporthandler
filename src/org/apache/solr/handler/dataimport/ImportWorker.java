package org.apache.solr.handler.dataimport;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.dataimport.DataImporter.Status;
import org.apache.solr.handler.dataimport.FlatTableImportHandler.PropertyKind;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/*
 * ImportWorker does the heavy lifting for migrating
 * the result of a MySQL query to Solr
 */
public class ImportWorker extends Thread {

	final SolrWriter sw;
	final Set<PropertyKind> propertyKinds;
	final String mergeColumn;
	final String query;
	// A set of Javascript functions are defined in the solrconfiguration
	// that allows modifying properties before they are added
	final Invocable scripts;
	static final MySQLInterface mysql = new MySQLInterface();
	Status status;
	private Exception causeOfFailure = null;
	// Toggle whether the quantity of dynamic properties
	// should be stored in the document as a field
	final boolean countProperties;
	// Toggle whether the dynamic property names
	// should be stored in the document as a field
	final boolean aggregatePropertyNames;
	Set<String> aggregatedPropertyNames = new HashSet<String>();
	
	int nrOfRows, nrOfDocs;
	long procTime;
	private boolean gracefulStop;

	public Status getStatus() { return status; }
	public Exception getCauseOfFailure() { return causeOfFailure; }
	public int getNrOfRowsProcessed() { return nrOfRows; }
	public int getNrOfDocsAdded() { return nrOfDocs; }
	public long getTimeSpentRunning() { return procTime; }

	public ImportWorker(
			SolrWriter sw, 
			Set<PropertyKind> propertyKinds,
			NamedList<String> optionalFields, 
			String mergeColumn, 
			String query,
			String scripts) throws Exception {
		Exception e = mysql.getError();
		if (e != null) {
			throw e;
		}
		this.sw = sw;
		this.propertyKinds = propertyKinds;
		this.mergeColumn = mergeColumn;
		this.query = query;
		this.status = Status.IDLE;
		this.gracefulStop = false;
		this.aggregatePropertyNames 
			= Boolean.parseBoolean(optionalFields.get("propertyNames"));
		this.countProperties 
			= Boolean.parseBoolean(optionalFields.get("propertyQuantity"));
		this.scripts = initInvocable(scripts);
	}

	private Invocable initInvocable(String script) throws Exception {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		engine.eval(script);
		return (Invocable) engine;
	}

	// Main import loop
	// for each row, as long as the unique id, or "merge name",
	// is the same, maintain a Solr document. Once the id changes,
	// submit the document to Solr and start a new one.
	public void run() {
		status = Status.RUNNING_FULL_DUMP;
		causeOfFailure = null;
		long time = System.currentTimeMillis() / 1000;

		try {

			ResultSet rs = mysql.fetch(query);

			// Acquire column names of columns that doesn't require special
			// treatment
			Set<String> nonPropertyColumns = acquireConventionalColNames(rs);

			nrOfRows = 0;
			nrOfDocs = 0;
			String mergeNameOfDocument = null;
			SolrInputDocument doc = null;
			while (rs.next()) {
				String mergeNameOfRow = rs.getString(mergeColumn);

				// Handle merge column and document creation
				if (mergeNameOfDocument == null
						|| !mergeNameOfDocument.equals(mergeNameOfRow)) {
					if (doc != null) {
						doc = addGeneratedFields(doc);
						sw.upload(doc);
						nrOfDocs++;
					}

					doc = new SolrInputDocument();
					mergeNameOfDocument = mergeNameOfRow;
					doc.addField(mergeColumn, mergeNameOfRow);

					/*
					 * Handle conventional columns: Currently they are not
					 * expected to be multi-valued and so are only added once
					 */
					for (String col : nonPropertyColumns) {
						try {
							doc.addField(col, rs.getObject(col));
						} catch (SQLException e) {
							throw new Exception("Could not find column " + col
									+ "\n" + "Availible columns are: "
									+ prettyPrint(nonPropertyColumns));
						}
					}
				}

				// Handle property columns
				for (PropertyKind pk : propertyKinds) {
					String propName = rs
							.getString(pk.propertyNameColumn);
					if (propName == null) {
						continue;
					}
					String propVal = (String) rs
							.getObject(pk.propertyValueColumn);

					// Use script transform on property pair
					if (pk.transformName != null) {
						String[] transProps = (String[]) scripts
								.invokeFunction(pk.transformName, propName, propVal);
						propName = transProps[0];
						propVal = transProps[1];
					}
					// Add the prefix for matching to a dynamic property
					propName = pk.outputPrefix + propName;

					SolrInputField field = doc.getField(propName);

					if (field == null) {
						doc.addField(propName, propVal);
						aggregatedPropertyNames.add(propName);

					} else if (!field.getValues().contains(propVal)) {
						if (pk.multiValued) {
							doc.addField(propName, propVal);
							aggregatedPropertyNames.add(propName);

						} else {
							/* Ending up here means that we are trying to set an
							 * additional value of a property that isn't
							 * declared as multi valued. No sensible action to
							 * take.
							 * 
							 * System.out.println(
							 * "[FlatTableImportHandler] Warning: additional property value of "
							 * +propName+
							 * "was skipped since property was declared as non multivalued.");
							 */
						}
					}
				}
				procTime = System.currentTimeMillis() / 1000 - time;

				if (this.gracefulStop /*|| nrOfDocs >= 10000*/) {
					break;
				}

				nrOfRows++;
			}
			if (doc != null) {
				sw.upload(doc);
				nrOfDocs++;
			}

			// A commit will cause the documents to
			// appear in searches. It is allegedly
			// a costly operation. Currently it is only done
			// once the whole query has been processed.
			sw.commit(true);
			sw.close();
			procTime = System.currentTimeMillis() / 1000 - time;
			status = Status.IDLE;

		} catch (Exception e) {
			status = Status.JOB_FAILED;
			causeOfFailure = e;
			return;
		}
	}

	// The worker should stop working and commit changes
	public void gracefulStop() {
		this.gracefulStop = true;
	}
	
	// Add aggregated property names to the document, 
	// giving a summary of the fields of the document
	// This allows some additional faceting tricks.
	private SolrInputDocument addGeneratedFields(SolrInputDocument doc) {

		if (aggregatePropertyNames) {
			doc.addField("propertyNames", aggregatedPropertyNames);
		}
		if (countProperties) {
			doc.addField("propertyQuantity", aggregatedPropertyNames.size());
		}
		aggregatedPropertyNames = new HashSet<String>();
		return doc;
	}

	private String prettyPrint(Set<String> set) {
		String rs = null;
		for (String s : set) {
			if (rs == null) {
				rs = "{" + s;
			} else {
				rs += ", " + s;
			}
		}
		rs += "}";
		return rs;
	}

	// Acquire column names from MySql that aren't to be treated as
	// dynamic properties
	private Set<String> acquireConventionalColNames(ResultSet rs)
			throws Exception {

		ResultSetMetaData rsMetaData = rs.getMetaData();
		HashSet<String> colNames = new HashSet<String>();
		Set<String> propColNames = acquireNonConventionalColNames();

		int colCount = rsMetaData.getColumnCount();

		int i = colCount;
		while (i > 0) {

			try {
				// Column indexing is ranging from 1 to length
				// _not_ 0 to length-1
				String colName = rsMetaData.getColumnLabel(i);

				if (!propColNames.contains(colName)
						&& !colName.equals(mergeColumn)) {
					colNames.add(colName);
				}
				i--;
			} catch (SQLException e) {
				throw new Exception("SQLException when accessing column " + i,
						e);
			}
		}
		return colNames;
	}

	// Acquire column names that are to be treated as
	// dynamic properties
	private Set<String> acquireNonConventionalColNames() {
		HashSet<String> rs = new HashSet<String>();
		for (PropertyKind pk : propertyKinds) {
			rs.add(pk.propertyNameColumn);
			rs.add(pk.propertyValueColumn);
		}
		return rs;
	}

	static class MySQLInterface {
		private Exception e = null;

		public MySQLInterface() {
			super();
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				this.e = e; 
			}
		}

		public Exception getError() {
			return this.e;
		}

		public ResultSet fetch(String query) throws SQLException {
			try {
				Connection dbConnection = DriverManager.getConnection(
						"jdbc:mysql://localhost/megabiz", "root", "qalixa");
				dbConnection.setAutoCommit(false);
				Statement statement = dbConnection.createStatement();
				statement.setFetchSize(Integer.MIN_VALUE);
				ResultSet resultSet = statement.executeQuery(query);
				return resultSet;
			} catch (SQLException e) {
				throw e;
			}

		}
	}
}
