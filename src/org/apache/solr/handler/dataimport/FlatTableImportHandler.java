package org.apache.solr.handler.dataimport;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.params.UpdateParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.RequestHandlerBase;
import org.apache.solr.handler.dataimport.DataImporter.Status;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorChain;
import org.apache.solr.util.plugin.SolrCoreAware;

/*
 * FlatTableImportHandler is the interface towards Solr for importing data 
 * from MySQL to Solr 
 */
public class FlatTableImportHandler extends RequestHandlerBase implements SolrCoreAware {
	// Each property kind describes a pair of columns in the MySql 
	// database that together hold property, value pairs.
	static class PropertyKind implements Serializable {
		private static final long serialVersionUID = 1668360863779276889L;
		// A prefix to be added to the property name in order
		// for it to match to its dynamic property field in the schema
		final String outputPrefix;
		// Name of the column storing property names
		final String propertyNameColumn;
		// Name of the column storing values
		final String propertyValueColumn;
		// Are these properties multi valued?
		final Boolean multiValued;
		// Name of javascript function (defined in the solr configuration)
		// used for modifying the property before adding it to a document
		final String transformName;
		public PropertyKind (
				String outputPrefix,
				String propertyNameColumn,
				String propertyValueColumn, 
				Boolean multiValued, 
				String transformName) {
			this.outputPrefix=outputPrefix;
			this.propertyNameColumn=propertyNameColumn; 
			this.propertyValueColumn=propertyValueColumn; 
			this.multiValued=multiValued;
			this.transformName=transformName;
		}
	}
	
	Set<PropertyKind> propertyKinds;
	private ImportWorker worker=null;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void init (NamedList args) {
		super.init(args);
		setUpPropertyKinds();
	}
	
	// Called by Solr whenever there is an url request to this module
	@Override
	public void handleRequestBody(SolrQueryRequest req, SolrQueryResponse resp)
			throws Exception {
		
		SolrParams params = req.getParams();
		SolrWriter sw = getSolrWriter(req, params, resp);
		
		processCommand(sw, params,resp);
	}

	// Create SolrWriter - that allows writing to index
	private SolrWriter getSolrWriter(SolrQueryRequest req, SolrParams params, SolrQueryResponse resp) {
		UpdateRequestProcessorChain processorChain =
                req.getCore().getUpdateProcessingChain(params.get(UpdateParams.UPDATE_CHAIN));
		UpdateRequestProcessor processor = processorChain.createProcessor(req, resp);
		return new SolrWriter(processor);
	}
	
	// Report current status and start new job if requested
	private void processCommand(SolrWriter sw, SolrParams params, SolrQueryResponse resp) 
			throws Exception {
		String command = params.get("command");
		if (command!=null && command.equals("full-import")){
			startWorker(sw, resp);
		} else if (command!=null && command.equals("stop")){
			stopWorker(resp);
		}
		pollWorker(resp);
	}
	
	// Initialize the worker
	@SuppressWarnings("unchecked")
	private void startWorker(SolrWriter sw, SolrQueryResponse resp) throws Exception {
		worker = new ImportWorker(sw, 
				propertyKinds,
				(NamedList<String>) initArgs.get("optionalFields"),
				(String) initArgs.get("mergeColumn"),
				(String) initArgs.get("query"),
				(String) initArgs.get("scripts"));
		worker.start();
	}
	
	// Tell the worker to gracefully stop and 
	// commit the work done so far
	private void stopWorker(SolrQueryResponse resp) throws Exception {
		if (worker != null) {
			worker.gracefulStop();
		}
	}

	// Ask worker on current status
	private void pollWorker(SolrQueryResponse resp) throws Exception {
		if (worker != null) {
			Status st = worker.getStatus();
			if(st==Status.JOB_FAILED){
				throw worker.getCauseOfFailure();
			}
			resp.add("status", st);
			resp.add("nrRowsProcessed", worker.getNrOfRowsProcessed());
			resp.add("nrDocsAdded", worker.getNrOfDocsAdded());
			resp.add("timeSpentWorking", worker.getTimeSpentRunning());
		} else {
			resp.add("status", "Nothing to do");
		}
	}

	@SuppressWarnings("unchecked")
	private void setUpPropertyKinds() {
		propertyKinds = new HashSet<PropertyKind>();
		
		Iterator<Entry<String, NamedList<String>>> propertyKindDefs
			= ((NamedList<NamedList<String>>) initArgs.get("propertyKinds")).iterator();
		while(propertyKindDefs.hasNext()){
			NamedList<String> propertyKindArguments = propertyKindDefs.next().getValue();
			PropertyKind pk = new PropertyKind(propertyKindArguments.get("outputPrefix"), 
					propertyKindArguments.get("propertyNameColumn"), 
					propertyKindArguments.get("propertyValueColumn"),
					Boolean.parseBoolean(propertyKindArguments.get("multiValued")),
					propertyKindArguments.get("scriptTransform")
					);
			propertyKinds.add(pk);
		}
	}


	
	// Unused interface methods
	@Override
	public void inform(SolrCore core) {}
	@Override
	public String getDescription() {return null;}
	@Override
	public String getSource() {return null;}
	@Override
	public String getSourceId() {return null;}
	@Override
	public String getVersion() {return null;}
}
